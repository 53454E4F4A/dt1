-- TEST CASE
-- DTP A1
-- Gruppe: S2T4
-- Jonas Sch�ufler, Daniel J�ger

library work;
    use work.all;
library ieee;
    use ieee.std_logic_1164.all;
    

entity testcase is
    port (
       test_out : out std_logic_vector(3 downto 0)
    );
end testcase;


architecture arc of testcase is
begin
    process
    begin
    
        test_out <= "0000";  
        wait for 10 ns;
        
        test_out <= "0001";
        wait for 10 ns;
        
        test_out <= "0010";  
        wait for 10 ns;
        
        test_out <= "0011";  
        wait for 10 ns;
        
        test_out <= "0100";  
        wait for 10 ns;
        
        test_out <= "0101";  
        wait for 10 ns;
        
        test_out <= "0110";  
        wait for 10 ns;
        
        test_out <= "0111";  
        wait for 10 ns;
        
        test_out <= "1000";  
        wait for 10 ns;
        
        test_out <= "1001";  
        wait for 10 ns;
        
        test_out <= "1010";  
        wait for 10 ns;
        
        test_out <= "1011";  
        wait for 10 ns;
        
        test_out <= "1100";  
        wait for 10 ns;
        
        test_out <= "1101";  
        wait for 10 ns;
        
        test_out <= "1110";  
        wait for 10 ns;
        
        test_out <= "1111";  
        wait for 10 ns;
        
        wait;
    end process;
end architecture arc;