-- TESTFRAME
-- DTP A1
-- Gruppe: S2T4
-- Jonas Sch�ufler, Daniel J�ger

library work;
    use work.all;
library ieee;
    use ieee.std_logic_1164.all;


entity testframe is
end entity testframe;


architecture beh of testframe is
    signal wire_i : std_logic_vector(3 downto 0);
    signal wire_o : std_logic;
    
    component evenparitychecker is
        port (
            parity_in : in std_logic_vector(3 downto 0);
            parity_out : out std_logic
        );
    end component evenparitychecker;
    for all : evenparitychecker use entity work.evenparitychecker(arc);
    
    component testcase is
        port (
           test_out : out std_logic_vector(3 downto 0)
        );
    end component testcase ;
    for all : testcase  use entity work.testcase (testcase);
    
begin
    wi : testcase 
        port map (
           test_out => wire_i      
        );
                
    wio : evenparitychecker
        port map (
           parity_in => wire_i,
          parity_out => wire_o
        );
end architecture beh;
