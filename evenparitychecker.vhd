-- EVEN PARITY CHECKER
-- DTP A1
-- Gruppe: S2T4
-- Jonas Sch�ufler, Daniel J�ger


library work;
    use work.all;
library ieee;
    use ieee.std_logic_1164.all;
    

entity evenparitychecker is
  port (
    parity_in : in std_logic_vector(3 downto 0);
    parity_out : out std_logic
        );
end evenparitychecker;
    
    
architecture arc of evenparitychecker is   
begin
  process (parity_in) is
    variable res : std_logic;
  begin
    res :=  not (parity_in(0) xor parity_in(1)) xor (parity_in(2) xor parity_in(3));
    parity_out <= res;	
  end process;
end architecture arc;
    
