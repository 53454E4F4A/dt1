onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider -height 50 input
add wave -noupdate -radix unsigned -childformat {{/testframe/wire_i(3) -radix unsigned} {/testframe/wire_i(2) -radix unsigned} {/testframe/wire_i(1) -radix unsigned} {/testframe/wire_i(0) -radix unsigned}} -subitemconfig {/testframe/wire_i(3) {-radix unsigned} /testframe/wire_i(2) {-radix unsigned} /testframe/wire_i(1) {-radix unsigned} /testframe/wire_i(0) {-radix unsigned}} /testframe/wire_i
add wave -noupdate /testframe/wire_i
add wave -noupdate -divider -height 50 output
add wave -noupdate /testframe/wire_o
TreeUpdate [SetDefaultTree]
quietly WaveActivateNextPane
WaveRestoreCursors {{Cursor 1} {30000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {168 ns}
